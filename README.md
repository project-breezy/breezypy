# Dota 2 AI Server

Here we show a random agent, which just sends random actions to the Breezy Server, as well as a more intelligent agent, using the Tangled Program Graph (TPG) algorithm. You can use either of these as a starting point for your own agent.

The random agent is more bare bones and therefore easier to add on to / build off of, the TPG agent is there just to show an example of what can be done. A real-valued, multi-output TPG agent is also available.

In order to use the TPG agent, you will need to install PyTPG found [here](https://github.com/Ryan-Amaral/PyTPG).

## Command Line Options:
The following options can just be left default, unless there are conflicts with other programs for instance.

- `-i` or `--ip`:  The ip where this agent server runs from (default: localhost).
- `-p` or `--port`: The port where this agent server runs from (default: 8086).
- `--ipb`: The ip where the Breezy Server is running at (default: localhost).
- `--portb`: The port where the Breezy Server is running at (default: 8085).

Extra options are used in the TPG example which are just hyperparameters of the algorithm, or different options for the run.
