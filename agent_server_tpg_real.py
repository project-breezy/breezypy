from http.server import BaseHTTPRequestHandler, HTTPServer
import json
from optparse import OptionParser
import threading
import requests
from tpg.trainer import Trainer
from tpg.agent import Agent
import numpy as np
from numpy import clip
import datetime
import math

parser = OptionParser()
# the address of this agent server
parser.add_option("-i", "--ip", type="string", dest="agentIp", default="127.0.0.1")
parser.add_option("-p", "--port", type="string", dest="agentPort", default="8086")

# the address of the breezy server
parser.add_option("--ipb", type="string", dest="breezyIp", default="127.0.0.1")
parser.add_option("--portb", type="string", dest="breezyPort", default="8085")

# continue a run
parser.add_option("-t", "--timestamp", type="string", dest="timestamp", default=None)

"""
Arguments for TPG setup.
"""
# population size
parser.add_option("-P", "--pop", type="int", dest="popSize", default=200)
parser.add_option("-g", "--gens", type="int", dest="gens", default=100)
parser.add_option("-r", "--gameReps", type="int", dest="gameReps", default=3)

(opts, args) = parser.parse_args()


"""
Handles HTTP requests from the dota server.
"""
class ServerHandler(BaseHTTPRequestHandler):

    """
    Get the fitness from the final game state.
    """
    def getFitness(self, state, win, died, kills):
        fitness = 10*state[24] + 150*kills + 150*(not died) + 2000*win
        
        # penalize for no enemy engagement (base camping)
        if state[24] == 0 and kills == 0 and not died:
            fitness -= 1000
            
        return fitness
        

    """
    Helper function to get content passed with http request.
    """
    def getContent(self):
        cLen = int(self.headers["Content-Length"])
        return self.rfile.read(cLen)

    """
    Sends a response containing a json object (usually the action).
    """
    def postResponse(self, response):
        # response code and info
        self.send_response(200)
        self.send_header("Content-type", "text/json")
        self.end_headers()
        # set the actual response data
        self.wfile.write(str(response).encode("utf-8"))

    """
    GET used for testing connection.
    """
    def do_GET(self):
        print(self.path)
        print("Received GET request.")
        self.postResponse(json.dumps({"response":"Hello"}))

    """
    POST used for getting features and returning action.
    """
    def do_POST(self):
    
        # agent used in either path
        global agent
        global lastState
        global died
        
        if self.path == "/update": 
            """
            Update route is called, game finished.
            """
            
            global agentScores
            global breezyIp
            global breezyPort
            
            print("Game done.")
            content = self.getContent().decode("utf-8")
            print(content)
            runData = json.loads(content)


            # may have to restart game if error occured
            if "error" in runData:
                print("Retarting rep #{}.".format(runData["progress"]))
                
                webhookUrl = "http://{}:{}{}".format(
                    breezyIp, breezyPort, runData["webhook"])
                
                # call webhook to trigger new game
                response = requests.get(url=webhookUrl)
            
                # send whatever to server
                self.postResponse(json.dumps({"nothing":0}))

            
            # save score to list of scores for current agent
            curFitness = self.getFitness(
                lastState, runData["winner"] == "Radiant", died,
                runData["radiantKills"])
            agentScores.append(curFitness)
            print("Agent scored {}!".format(curFitness))
            
            died = False
            
            # webhook to start new game in existing set of games
            if "webhook" in runData:
                """
                A webhook was sent to the agent to start a new game in the current
                set of games.
                """
                
                print("Starting rep #{}.".format(runData["progress"]+1))
                
                webhookUrl = "http://{}:{}{}".format(
                    breezyIp, breezyPort, runData["webhook"])
                
                # call webhook to trigger new game
                response = requests.get(url=webhookUrl)
                
            # otherwise start new set of games, or end session
            else:
                """
                This sample agent just runs indefinately. So here I will just start
                a new set of 5 games. You could just always set the amount of games
                to 1, and forget about the webhook part, whatever works for you.
                In here would probably be where you put the code to ready a new agent
                (update NN weights, evolutions, next agent in current gen. etc.).
                """
                
                global trainer
                global agents
                global totalGens
                global gameReps
                global curGen
                global logName
                
                """
                Prepare next TPG agent (or generation if required).
                """
                
                if curGen == totalGens:
                    print("Done Training.")
                    return
                    
                # reward score to current agent
                fitness = sum(agentScores)/gameReps
                agent.reward(fitness, "dota")
                print("Agent done. Fitness: {}.".format(fitness))
                agentScores = []
                
                # log the current score
                with open(logName, "a") as f:
                    f.write("{},{}\n".format(curGen, fitness))
                
                if len(agents) == 0:
                    curGen += 1
                    print("On to generation #{}.".format(curGen))
                    
                    global trainerFileName
                    
                    # start new generation
                    trainer.evolve(tasks=["dota"])
                    trainer.saveToFile(trainerFileName)
                    
                    # get new agents
                    agents = trainer.getAgents(skipTasks=["dota"])
                    
                    
                # get next agent
                agent = agents.pop()
                
                
                
                # build url to dota 2 breezy server
                startUrl = "http://{}:{}/run/".format(
                    breezyIp, breezyPort)
                # create a run config for this agent, to run 5 games
                startData = {
                    "agent": "Sample TPG Agent",
                    "size": gameReps,
                    "relayMode": "async",
                    "timescale": 10
                }
                response = requests.post(url=startUrl, data=json.dumps(startData))
                
                
                
            # send whatever to server
            self.postResponse(json.dumps({"fitness":curFitness}))
            
        else: # relay path gives features from current game to agent
            """
            Relay route is called, gives features from the game for the agent.
            """
            
            # get data as json, then save to list
            content = self.getContent().decode("utf-8")
            features = json.loads(content)
            
            lastState = features # save last state to calculate fitness at end
            
            if features[2] <= 0:
                died = True
        
            """
            Agent code to determine action from features goes here.
            """
            # action[0] is action code (irrelevant in this case)
            # action[1] is the action action vector e.g. [a1, a2, a3, a4, a5]
            action = agent.act(np.array(features, dtype=np.float64))[1]
            action = clip(action, -500, 500)
            # round actions to not upset Lua
            self.postResponse(json.dumps({"actionCode":[round(action[0], 10),
                                                        round(action[1], 10),
                                                        round(action[2], 10),
                                                        round(action[3], 10),
                                                        round(action[4], 10)]}))

if __name__ == "__main__":
    """
    Sets up and starts the Agent server and triggers the start of a run on the 
    Breezy server.
    """

    # start the Agent server in other thread
    print("Agent Server starting at {}:{}...".format(opts.agentIp, opts.agentPort))
    agentHandler = HTTPServer((opts.agentIp, int(opts.agentPort)), ServerHandler)
    thread = threading.Thread(target=agentHandler.serve_forever)
    thread.daemon = True
    thread.start()
    print("Agent server started.")
    
    """
    Declare variables global that you want the agent server to have access to.
    Also initialization.
    """
    # from options
    global breezyIp
    global breezyPort
    global totalGens
    global gameReps
    
    # for agent
    global trainer
    global agents
    global agent
    global agentScores
    global curGen
    global lastState
    
    # other
    global died
    
    
    breezyIp = opts.breezyIp
    breezyPort = opts.breezyPort
    totalGens = opts.gens
    gameReps = opts.gameReps
    
    # create a log file 
    global logName
    global trainerFileName
    if opts.timestamp is None:
        # start of new run
        timestamp = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")
        trainerFileName = "trainer-{}.pkl".format(timestamp)
        # set up of the TPG agent
        # action=[5,5] means 2 real valued action vectors each of size 5.
        # Theoretically only one is needed but currently TPG has a limitation
        # where it requires at least two different actions. This has no effect
        # with real valued actions of the same length.
        trainer = Trainer(actions=[5,5], 
                      teamPopSize=opts.popSize,
                      rootBasedPop=False,
                      inputSize=310)
        curGen = 0
    else:
        # continue from old run
        timestamp = opts.timestamp
        trainerFileName = "trainer-{}.pkl".format(timestamp)
        trainer = loadTrainer(trainerFileName)
        curGen = trainer.generation
        
    logName = "log-{}.txt".format(timestamp)
    
    # precompile  action function
    trainer.getAgents()[0].act(np.zeros(310))
    
    agents = trainer.getAgents()
    agent = agents.pop()
    agentScores = []
    lastState = None
    
    died = False
    
    """
    Start the server and tell Breezy Server we are ready.
    """
    
    # create a run config for this agent, to run 5 games, send to breezy server
    startData = {
        "agent": "Sample TPG Agent",
        "size": opts.gameReps,
        "relayMode": "async",
        "timescale": 10
    }
    # tell breezy server to start the run
    response = requests.post(
        url="http://{}:{}/run/".format(opts.breezyIp, opts.breezyPort), 
        data=json.dumps(startData))
    
    print(response)

    # serve until force stop
    while True:
        pass
    
